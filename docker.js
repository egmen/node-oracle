import { loadJson, log } from "./helpers.js";

async function getTagList(repositoryName = "", tag = "") {
  const url = new URL(
    `https://hub.docker.com/v2/repositories/library/${repositoryName}/tags/`
  );
  url.searchParams.append("page_size", "100");
  url.searchParams.append("page", "1");
  url.searchParams.append("name", tag);

  const data = await loadJson(url.href);
  return data.results;
}

export async function getLatestTagList(
  repositoryName = "",
  tags = [""],
  suffixes = [""]
) {
  const resultList = [];
  for (const tag of tags) {
    log(`loading docker "${tag}" tag`);
    resultList.push(...(await getTagList(repositoryName, tag)));
  }
  const joined = new Map();
  resultList.forEach((item) => {
    const foundImage = item.images.find((img) => img.architecture === "amd64");
    if (!foundImage) {
      const archList = item.images.map(({ architecture }) => architecture);
      log(
        `no "amd64" architecture image: ${item.name}, only ${archList.join(
          " ,"
        )} available`
      );
      return;
    }
    const { digest } = foundImage;

    if (!joined.has(digest)) {
      joined.set(digest, new Set());
    }
    joined.get(digest).add(item.name);
  });

  const tagCombination = tags.reduce(
    (acc, prefix) => [
      ...acc,
      ...suffixes.map((suffix) => `${prefix}${suffix}`),
    ],
    []
  );
  const filterResult = Array.from(joined).filter(([_, dockerTags]) => {
    return tagCombination.find((tag) => {
      if (dockerTags.has(tag)) {
        return true;
      }
      return false;
    });
  });
  return new Map(filterResult);
}
