ARG NODE_VERSION
FROM node:${NODE_VERSION}
ENV LD_LIBRARY_PATH="/opt/oracle/instantclient"
ENV OCI_HOME="/opt/oracle/instantclient"
ENV OCI_LIB_DIR="/opt/oracle/instantclient"
# ENV OCI_INCLUDE_DIR="/opt/oracle/instantclient/sdk/include"
# ENV OCI_VERSION=12
RUN apt-get update \
    && apt-get install -y libaio1 unzip wget \
    && mkdir -p /opt/oracle \
    && mkdir -p /tmp/oracle \
    && cd /tmp/oracle \
    && wget https://download.oracle.com/otn_software/linux/instantclient/instantclient-basic-linuxx64.zip \
    && unzip instantclient-basic-linuxx64.zip \
    && mv $(find . -maxdepth 1 -type d | sed -e 's/[. /]//g') $LD_LIBRARY_PATH \
    && apt-get clean autoclean \
    && apt-get autoremove -y \
    && rm -rf /tmp/* \
    && rm -rf /var/lib/{apt,dpkg,cache,log}/
