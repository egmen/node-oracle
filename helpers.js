import https from "https";

export function loadJson(url) {
  return new Promise((resolve, reject) => {
    let data = "";
    https
      .get(url, (res) => {
        res.on("data", (chunk) => (data += chunk.toString()));
        res.on("error", () => reject(err));
        res.on("end", () => {
          try {
            const json = JSON.parse(data);
            resolve(json);
          } catch (err) {
            reject(new Error(`Invalid JSON in response ${url}`));
          }
        });
      })
      .on("error", (err) => reject(err));
  });
}

export function getMainTag(tags = [""]) {
  const exactVersionShortLength = tags.reduce(
    (result, tag) =>
      (tag.match(/\./g) || []).length === 2 &&
      (!result || tag.length < result.length)
        ? tag
        : result,
    ""
  );
  const minLengthTag = tags.reduce((result, tag) =>
    tag.length < result.length ? tag : result
  );
  return exactVersionShortLength || minLengthTag;
}

export function log(text = "") {
  console.log(new Date().toISOString(), text);
}
