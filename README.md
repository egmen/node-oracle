# Project archived
From v6.0.0 no need external libraries any more to install node-oracle https://node-oracledb.readthedocs.io/en/latest/release_notes.html#node-oracledb-v6-0-0-24-may-2023

# NodeJS Docker image with Oracle Instant Client

DockerHub https://hub.docker.com/r/egmen/node-oracle

CI and repository https://gitlab.com/egmen/node-oracle

Image examples:

```bash
docker pull egmen/node-oracle:latest
docker pull egmen/node-oracle:lts
docker pull egmen/node-oracle:12.16.3
docker pull egmen/node-oracle:12
docker pull egmen/node-oracle:14.1-slim
```

## Build your own image

```bash
mkdir -p /tmp/node-oracle
cd /tmp/node-oracle
wget https://gitlab.com/egmen/node-oracle/-/raw/master/Dockerfile
NODE_VERSION=13-slim # tag from https://hub.docker.com/_/node?tab=description
docker build --build-arg NODE_VERSION=$NODE_VERSION --tag node-oracle:$NODE_VERSION .

```

## Dockerfile

https://gitlab.com/egmen/node-oracle/-/blob/master/Dockerfile

## Update policy

Rebuilds every week for every actual NodeJS versions (major, minor, patch, lts, latest and -slim ones) from https://hub.docker.com/_/node?tab=description and actual Oracle Instant Client from https://www.oracle.com/database/technologies/instant-client/linux-x86-64-downloads.html

## Testing

Not implemented

[Need help](https://gitlab.com/egmen/node-oracle/-/issues/1) for access any Oracle Database to deploy images only after successfull test-stage.
