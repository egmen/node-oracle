import fs from "fs";

import { loadJson, getMainTag, log } from "./helpers.js";
import { getLatestTagList } from "./docker.js";

const NODE_JS_VERSIONS =
  "https://raw.githubusercontent.com/nodejs/Release/master/schedule.json";

Promise.resolve()
  .then(async function nodeVersionList() {
    log("loading NodeJS version list");
    const isoToday = new Date().toISOString().split("T")[0];
    const isoPreviousWeek = new Date(Date.now() - 1000 * 60 * 60 * 24 * 7)
      .toISOString()
      .split("T")[0];
    const data = await loadJson(NODE_JS_VERSIONS);
    const nodeVersions = Object.entries(data)
      .map(([version, value]) => ({
        version,
        prefix: version.replace("v", ""),
        ...value,
      }))
      .filter((ver) => {
        if (ver.start < isoPreviousWeek) {
          // Arleady started
          if (ver.end > isoToday) {
            // Currently maintained
            return true;
          }
        }
        return false;
      });
    return nodeVersions;
  })
  .then(async (nodeVersions) => {
    const prefixes = nodeVersions.map((item) => item.prefix);
    const nodeTags = await getLatestTagList(
      "node",
      ["lts", "latest", "current", ...prefixes],
      ["", "-slim"]
    );
    const forBuild = Array.from(nodeTags).map(([digest, tags]) => {
      const stageSuffix = digest.substring(7, 15);
      return {
        digest,
        stageSuffix,
        nodeVersion: getMainTag([...tags]),
        tags: [...tags],
      };
    });
    return forBuild;
  })
  .then(function preparePipelineFile(forBuild) {
    const pipelineFilename = "build-trigger.yml";
    const template = fs.readFileSync("build-template.yml", "utf-8");
    // forBuild
    //   .map((ver) =>
    //     [
    //       `build-node-${ver.stageSuffix}:`,
    //       "  stage: triggers",
    //       "  variables:",
    //       `    NODE_VERSION: ${ver.nodeVersion}`,
    //       `    TAG_LIST: ${ver.tags.join(" ")}`,
    //       "  trigger:",
    //       "    include: build-pipeline.yml",
    //       "    strategy: depend",
    //       "",
    //     ].join("\n")
    //   )
    //   .join("\n");
    const yamlContent = [
      `services:\n  - docker:19.03.8-dind\n`,
      ...forBuild.map((ver) =>
        template
          .replace(new RegExp("\\$NODE_VERSION", "g"), ver.nodeVersion)
          .replace(new RegExp("\\$TAG_LIST", "g"), ver.tags.join(" "))
      ),
    ].join("\n");
    fs.writeFileSync(pipelineFilename, yamlContent);
    log("all done");
  })
  .catch((err) => {
    setImmediate(() => {
      throw err;
    });
  });
