const dotenv = require("dotenv");
const oracledb = require("oracledb");

dotenv.config();
oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;

Promise.resolve()
  .then(async () => {
    const pool = await oracledb.createPool({
      connectionString: process.env.DATABASE_ENTRYPOINT,
      user: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
      poolMax: 10,
    });
    const connection = await pool.getConnection();
    const result = await connection.execute(
      "SELECT 11 + 52 AS TEST_OK FROM DUAL"
    );
    await connection.close();
    console.log("NodeJS", process.version);
    console.log(result.rows[0]);
  })
  .catch((err) =>
    setImmediate(() => {
      throw err;
    })
  );
